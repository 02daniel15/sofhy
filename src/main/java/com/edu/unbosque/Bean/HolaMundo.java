package com.edu.unbosque.Bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class HolaMundo {
	
	private String  hola = "Hola Mundo JSF";
	
	

	public String getHola() {
		return hola;
	}

	public void setHola(String hola) {
		this.hola = hola;
	}
	
	

}
